const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-hemadriv1:vinay20199609@cca-hemadriv1.4mphd.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err => {
if (err) throw err;
console.log("Connected to the MongoDB cluster");
});
var port = process.env.PORT || 8080;
const cors = require('cors')
app.use(express.json())
app.use(cors())
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
app.listen(port, () =>
console.log(`HTTP Server with Express.js is listening on port:${port}`))
app.get('/', function (req, res) {
res.send("sprint 2 microservice by Vinay and Abhinav")
})
app.post("/login", function(req,res){
    const db = dbClient.db();
    const {username,password} = req.body
    console.log("got a login request : " + username + " " + password)
    db.collection("users").findOne({username:username,password:password},(err, user)=>{
    if (user){
    console.log(user)
    user.status = true
    delete user.password
    delete user._id
    res.send(user)
    }
    else{
    user = {}
    user.status = false
    res.send(user)
    }
    });
})
app.post("/signup", function(req,res){
    const db = dbClient.db();
    const {username,password,fullname,email} = req.body
    db.collection("users").findOne({username:username},(err, user)=>{
    if (user){
    user.signup = false
    res.send(user)
    }
    else{
    db.collection("users").insertOne(req.body,(err, user)=>{
    user.signup = true
    res.send(user)
    })
    }
    });
})
app.get("/signin/:username/:password",(req,res)=>{
    var username_1 = req.params.username
    var password_1 = req.params.password

    console.log('/signin->req.body.username:' + username_1)
    console.log('/signin->req.body.password:' + password_1)

    const db = dbClient.db();
    db.collection("users").findOne({username:username_1, password:password_1},(err, user)=>{
        if (err) throw err;

        if(user && user.username===username_1){
            console.log('user verified!!!' + username_1)
            res.send(user)
        }else{
            console.log('Login failed!')
            res.send("{error: Login failed}")
        }
    })
})


app.get("/registeruser/:username/:password/:fullname/:email",(req,res)=>{

    const db = dbClient.db();
        var myobj = { username: req.params.username,
            password: req.params.password,
             fullname: req.params.fullname,
              email: req.params.email }
    
        var error = { error: "Username exists! Please login or Create another user." }
    
        db.collection("users").findOne({username:req.params.username},(err, user)=>{
                if (err) throw err;
    
                if(user){
                    console.log(user)
                    res.send(error);
                }else{
                    db.collection("users").insertOne(myobj, function(err, result) {
                        if (err) throw err;
                        console.log("User registered successfully."); 
                        res.send(result)
                      })
                }
            })
})






